FROM docker:dind
RUN apk update && apk add curl
RUN mkdir -p ~/.docker/cli-plugins
RUN curl https://github.com/docker/scan-cli-plugin/releases/latest/download/docker-scan_linux_amd64 -L -s -S -o ~/.docker/cli-plugins/docker-scan
RUN chmod +x ~/.docker/cli-plugins/docker-scan
RUN apk add git

